import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  AnimationController _animationSizeController;
  AnimationController _animationOpacityController;

  Animation<double> _size;
  Animation<double> _opacity;
  Animation<Color> _color;
  bool isTapped = false;

  @override


  void initState() {
    _animationOpacityController = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 1500,
      ),
      reverseDuration: Duration(milliseconds: 1500),
    );
    _opacity = Tween<double>(begin: 0, end: 1).animate(_animationOpacityController);

    _animationSizeController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
      reverseDuration: Duration(milliseconds: 1000),
    );
    _size = Tween<double>(begin: 5, end: 300).animate(_animationSizeController);
    _color = ColorTween(begin: Colors.green, end: Colors.red)
        .animate(_animationSizeController);

    _animationSizeController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        setState(() {
          isTapped = !isTapped;
        });
        _animationOpacityController.forward();
      }
      if (status == AnimationStatus.reverse) {
        _animationOpacityController.reverse();
      }
      if (status == AnimationStatus.dismissed) {
        setState(() {
          isTapped = !isTapped;
        });
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lecture practice 4'),
      ),
      body: AnimatedBuilder(
        animation: _animationSizeController,
        builder: (BuildContext context, Widget _) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                alignment: Alignment.center,
                height: _size.value,
                width: double.infinity,
                color: Colors.orange,
                child: AnimatedBuilder(
                  animation: _animationOpacityController,
                  builder: (BuildContext context, Widget _) {
                    return Opacity(
                      opacity: _opacity.value,
                      child: Text('Here is some text',
                      style: TextStyle(
                        fontSize: 50,
                      ),),
                    );
                  },
                ),
              ),
              Container(
                height: 50,
                width: 150,

                child: Material(
                  color: _color.value,
                  child: InkWell(
                    onTap: () {
                      isTapped
                          ?  _animationSizeController.reverse()
                          : _animationSizeController.forward();
                      print(_animationSizeController.status);
                    },
                    child: Center(child: Text('Tap me!!!')),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
